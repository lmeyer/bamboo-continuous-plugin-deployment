var confDeploy = confDeploy || false;

(function($){

    var productDropDown$ = $("select#bcpd_config_product");
    if (productDropDown$.size() > 0) {
        updateProduct(productDropDown$); // set initial values for product drop-down
    }

    // This script gets included in-line with the task configuration form. This means that the script can be included and
    // executed multiple times within the lifetime of a single page-load (ie. each time a new task is edited or created,
    // the form will be rendered).
    //
    // To prevent doing all this binding multiple times on the same page (and causing re-entrant bugs), we need something
    // to short-circuit subsequent invocations.
    if (confDeploy)
    {
        console.log("Skipping task config");
        return;
    }
    confDeploy = true;

    function addFieldError(fieldInput, errorMessage) {
        var fieldWrapper = fieldInput.closest("fieldset, .field-group");
        fieldWrapper.append("<div class=\"error\">" + errorMessage + "</div>");
    }

    function removeFieldError(fieldInput) {
        var fieldWrapper = fieldInput.closest("fieldset, .field-group");
        $("div.error", fieldWrapper).remove();
    }

    function getValues() {

        var form = $("form#updateTask, form#createTask, form#createEnvironmentTask, form#updateEnvironmentTask"); // form has a different ID depending on 'create' or 'edit' context, and whether or not it's a build task or a deployment task

        var useAtlassianId = $("#bcpd_config_useAtlassianId", form).val() === "true";
        var useAtlassianIdWebSudo = $("#bcpd_config_useAtlassianIdWebSudo").val() === "true";
        var atlassianIdPasswordVariableCheck = $("#bcpd_config_useAtlassianIdPasswordVariable", form).val() === "true";
        var passwordVariableCheck = $("#bcpd_config_usePasswordVariable", form).val() === "true";

        return {
            useAtlassianId: useAtlassianId,
            atlassianIdBaseUrl: $("#bcpd_config_atlassianIdBaseURL", form).val(),
            username: useAtlassianId ? $("#bcpd_config_atlassianIdUsername", form).val() : $("#bcpd_config_username", form).val(),
            usePasswordVariable: useAtlassianId ? atlassianIdPasswordVariableCheck : passwordVariableCheck,
            password: useAtlassianId ? $("#bcpd_config_atlassianIdPassword", form).val() : $("#bcpd_config_password", form).val(),
            passwordVariable: useAtlassianId ? $("#bcpd_config_atlassianIdPasswordVariable", form).val() : $("#bcpd_config_passwordVariable", form).val(),
            webSudoAppName: useAtlassianId ? $("#bcpd_config_atlassianIdAppName", form).val() : "",
            overrideWebSudo: useAtlassianId ? !useAtlassianIdWebSudo : "",
            useWebSudoPasswordVariable: useAtlassianId && !useAtlassianIdWebSudo ? passwordVariableCheck : "",
            webSudoPassword: useAtlassianId && !useAtlassianIdWebSudo ? $("#bcpd_config_password", form).val() : "",
            webSudoPasswordVariable: useAtlassianId && !useAtlassianIdWebSudo ? $("#bcpd_config_passwordVariable", form).val() : ""
        };
    }

    function putValues(values) {

        var form = $("form#updateTask, form#createTask, form#createEnvironmentTask, form#updateEnvironmentTask"); // form has a different ID depending on 'create' or 'edit' context, and whether or not it's a build task or a deployment task

        if (values.useAtlassianId) {
            $("#bcpd_config_useAtlassianId", form).val("true");

            $("#bcpd_config_atlassianIdBaseURL", form).val(values.atlassianIdBaseUrl);
            $("#bcpd_config_atlassianIdUsername", form).val(values.username);
            $("#bcpd_config_useAtlassianIdPasswordVariable", form).val(values.usePasswordVariable);
            $("#bcpd_config_atlassianIdPassword", form).val(values.usePasswordVariable ? "" : values.password);
            $("#bcpd_config_atlassianIdPasswordVariable", form).val(values.usePasswordVariable ? values.password : "");

            $("#bcpd_config_username", form).val("");

            if (values.useAtlassianIdWebSudo) {
                $("#bcpd_config_useAtlassianIdWebSudo", form).val("true");
                $("#bcpd_config_atlassianIdAppName", form).val(values.webSudoAppName);

                $("#bcpd_config_usePasswordVariable", form).val("");
                $("#bcpd_config_password", form).val("");
                $("#bcpd_config_passwordVariable", form).val("");

            } else {
                $("#bcpd_config_useAtlassianIdWebSudo", form).val("false");
                $("#bcpd_config_atlassianIdAppName", form).val("");

                $("#bcpd_config_usePasswordVariable", form).val(values.useWebSudoPasswordVariable);
                $("#bcpd_config_password", form).val(values.useWebSudoPasswordVariable ? "" : values.webSudoPassword);
                $("#bcpd_config_passwordVariable", form).val(values.useWebSudoPasswordVariable ? values.webSudoPassword : "");
            }
        } else {
            $("#bcpd_config_useAtlassianId", form).val("false");

            $("#bcpd_config_username", form).val(values.username);
            $("#bcpd_config_usePasswordVariable", form).val(values.usePasswordVariable);
            $("#bcpd_config_password", form).val(values.usePasswordVariable ? "" : values.password);
            $("#bcpd_config_passwordVariable", form).val(values.usePasswordVariable ? values.password : "");

            $("#bcpd_config_atlassianIdBaseURL", form).val("");
            $("#bcpd_config_atlassianIdUsername", form).val("");
            $("#bcpd_config_atlassianIdPassword", form).val("");
            $("#bcpd_config_useAtlassianIdPasswordVariable", form).val("");
            $("#bcpd_config_atlassianIdPasswordVariable", form).val("");
            $("#bcpd_config_useAtlassianIdWebSudo", form).val("");
            $("#bcpd_config_atlassianIdAppName", form).val("");
        }

        var message = $("#stored-credentials-message");
        message.text(AJS.I18n.getText("bcpd.config.form.credentials.msg.some", [values.username]));
        message.addClass("description");
        message.removeClass("error");
    }

    // Handle updating the data attributes when the selected product is changed
    function updateProduct(field$) {
        var val = field$.val();

        field$.data("product-name", $("option:selected", field$).text());
        if (val === "bcpd.product.bamboo" || val === "bcpd.product.fecru" || val === "bcpd.product.stash")
        {
            field$.data("supports-websudo", false);
        }
        else
        {
            field$.data("supports-websudo", true);
        }
    }

    // handle product drop-down changes
    $(document).on("change", "#bcpd_config_product", function(event) {
        updateProduct($(this));
    });

    // Handle the "what does this setting do?" links
    $(document).on("click", ".bcpd-description-expander", function(event) {
        var link$ = $(this);
        if (link$.hasClass("expanded")) {
            link$.next().hide("slow");
            link$.removeClass("expanded");
        }
        else {
            link$.next().show("slow");
            link$.addClass("expanded");

        }

        event.preventDefault();
    });

    // Bind the edit credentials button, and future appearances of it.
    $(document).on("click", "#bcpd_config_credentials", function(event) {

        var loginPanel = {
            panelId: 0,
            fields: {
                _useProductLogin: null,
                useProductLogin: function() {
                    if (this._useProductLogin == null)
                        this._useProductLogin = $("#bcpd-login-useAtlassianId-productLogin");
                    return this._useProductLogin;
                },

                _useAtlassianId: null,
                useAtlassianId: function() {
                    if (this._useAtlassianId == null)
                        this._useAtlassianId = $("#bcpd-login-useAtlassianId-atlassianId");
                    return this._useAtlassianId;
                },

                _atlassianIdBaseUrl: null,
                atlassianIdBaseUrl: function() {
                    if (this._atlassianIdBaseUrl == null)
                        this._atlassianIdBaseUrl = $("#bcpd-login-atlassianIdBaseUrl");
                    return this._atlassianIdBaseUrl;
                },

                _username: null,
                username: function() {
                    if (this._username == null)
                        this._username = $("#bcpd-login-username");

                    return this._username;
                },

                _usePasswordVariable: null,
                usePasswordVariable: function() {
                    if (this._usePasswordVariable == null)
                        this._usePasswordVariable = $("#bcpd-login-usePasswordVariable");

                    return this._usePasswordVariable;
                },

                _password: null,
                password: function() {
                    if (this._password == null)
                        this._password = $("#bcpd-login-password");

                    return this._password;
                },

                _passwordVariable: null,
                passwordVariable: function() {
                    if (this._passwordVariable == null)
                        this._passwordVariable = $("#bcpd-login-passwordVariable");

                    return this._passwordVariable;
                }
            },

            validate: function() {

                var success = true;

                // Login method must be selected
                if (!this.fields.useAtlassianId().prop("checked") &&
                    !this.fields.useProductLogin().prop("checked"))
                {
                    addFieldError(this.fields.useAtlassianId(), AJS.I18n.getText("bcpd.config.dialog.login.method.error.required"));
                    success = false;
                }

                // Username is required
                if (!this.fields.username().val())
                {
                    addFieldError(this.fields.username(), AJS.I18n.getText("bcpd.config.dialog.login.username.error.required"));;
                    success = false;
                }

                // If 'Use Bamboo Variable' is selected, bamboo variable must have a value and must be like '${bamboo.*}'.
                if (this.fields.usePasswordVariable().prop("checked"))
                {
                    var val = this.fields.passwordVariable().val();
                    if (!val)
                    {
                        addFieldError(this.fields.passwordVariable(), AJS.I18n.getText("bcpd.config.dialog.login.password.error.required"));
                        success = false;
                    }
                    else
                    {
                        // Poor-man's .startsWith()
                        var expected = "${bamboo.";
                        if (!(val.substring(0, expected.length) === expected))
                        {
                            addFieldError(this.fields.passwordVariable(), AJS.I18n.getText("bcpd.config.dialog.login.password.error.format"));;
                            success = false;
                        }
                    }
                }
                else
                {
                    // If the 'Use Bamboo Variable' field is not selected, then the password field must have a value.
                    if (!this.fields.password().val())
                    {
                        addFieldError(this.fields.password(), AJS.I18n.getText("bcpd.config.dialog.login.password.error.required"));
                        success = false;
                    }
                }

                return success;
            },

            init: function () {
                if (this.fields.useAtlassianId().prop("checked")) {
                    webSudoPanel.enable();
                    loginPanel.fields.atlassianIdBaseUrl().prop("disabled", false);
                }
                else {
                    webSudoPanel.disable();
                    loginPanel.fields.atlassianIdBaseUrl().prop("disabled", true);
                }

                if (this.fields.usePasswordVariable().prop("checked")) {
                    loginPanel.fields.password().hide();
                    loginPanel.fields.passwordVariable().show();
                }
                else {
                    loginPanel.fields.password().show();
                    loginPanel.fields.passwordVariable().hide();
                }
            },

            wire: function(loginPanel$) {

                // Atlassian ID checkbox toggles WebSudo panel state.
                this.fields.useProductLogin().on("change", function() {
                    if ($(this).prop("checked"))
                    {
                        webSudoPanel.disable();
                        loginPanel.fields.atlassianIdBaseUrl().prop("disabled", true);
                    }
                });

                this.fields.useAtlassianId().on("change", function() {
                    if ($(this).prop("checked"))
                    {
                        webSudoPanel.enable();
                        loginPanel.fields.atlassianIdBaseUrl().prop("disabled", false);
                    }
                });

                // Don't hide the input characters if the user selects to use a bamboo variable for the password
                this.fields.usePasswordVariable().on("change", function() {

                    if ($(this).prop("checked"))
                    {
                        loginPanel.fields.password().hide();
                        loginPanel.fields.passwordVariable().show();
                    }
                    else
                    {
                        loginPanel.fields.password().show();
                        loginPanel.fields.passwordVariable().hide();
                    }
                });

                // Bind all the inputs to clear their errors when focussed
                $("input", loginPanel$).on("focus", function() {
                    var this$ = $(this);
                    removeFieldError(this$);
                });
            },

            saveValues: function(values) {
                values.useAtlassianId = this.fields.useAtlassianId().prop("checked") === true;

                values.atlassianIdBaseUrl = values.useAtlassianId ? this.fields.atlassianIdBaseUrl().val() : "";
                values.username = this.fields.username().val();
                values.usePasswordVariable = this.fields.usePasswordVariable().prop("checked") === true;
                values.password =  values.usePasswordVariable ? this.fields.passwordVariable().val() : this.fields.password().val();
            }
        };

        var webSudoPanel;
        if (AJS.$("#bcpd_config_product").data("supports-websudo"))
        {
            webSudoPanel = {
                enabled: false,
                panelId: 1,
                fields: {
                    _useProductWebSudo: null,
                    useProductWebSudo: function () {
                        if (this._useProductWebSudo == null)
                            this._useProductWebSudo = $("#bcpd-websudo-method-product");
                        return this._useProductWebSudo;
                    },

                    _useAtlassianIdWebSudo: null,
                    useAtlassianIdWebSudo: function () {
                        if (this._useAtlassianIdWebSudo == null)
                            this._useAtlassianIdWebSudo = $("#bcpd-websudo-method-lasso");
                        return this._useAtlassianIdWebSudo;
                    },

                    _webSudoAppName: null,
                    webSudoAppName: function () {
                        if (this._webSudoAppName == null)
                            this._webSudoAppName = $("#bcpd-websudo-appname");
                        return this._webSudoAppName;
                    },

                    _usePasswordVariable: null,
                    usePasswordVariable: function () {
                        if (this._usePasswordVariable == null)
                            this._usePasswordVariable = $("#bcpd-websudo-usePasswordVariable");
                        return this._usePasswordVariable;
                    },

                    _password: null,
                    password: function () {
                        if (this._password == null)
                            this._password = $("#bcpd-websudo-password");
                        return this._password;
                    },

                    _passwordVariable: null,
                    passwordVariable: function () {
                        if (this._passwordVariable == null)
                            this._passwordVariable = $("#bcpd-websudo-passwordVariable");
                        return this._passwordVariable;
                    }
                },

                validate: function () {
                    if (!this.enabled)
                        return true;

                    var success = true;

                    // WebSudo method must be selected
                    // Login method must be selected
                    if (!this.fields.useAtlassianIdWebSudo().prop("checked") && !this.fields.useProductWebSudo().prop("checked")) {
                        addFieldError(this.fields.useAtlassianIdWebSudo(), AJS.I18n.getText("bcpd.config.dialog.websudo.method.error.required"));
                        success = false;
                    }

                    // If 'use product websudo' is selected, a password must be provided
                    if (this.fields.useProductWebSudo().prop("checked")) {
                        // If the 'use password variable' option is selected, bamboo variable must have a value and must be like '${bamboo.*}'.
                        if (this.fields.usePasswordVariable().prop("checked")) {
                            var val = this.fields.passwordVariable().val();
                            if (!val) {
                                addFieldError(this.fields.passwordVariable(), AJS.I18n.getText("bcpd.config.dialog.login.password.error.required"));
                                success = false;
                            }
                            else {
                                // Poor-man's .startsWith()
                                var expected = "${bamboo.";
                                if (!(val.substring(0, expected.length) === expected)) {
                                    addFieldError(this.fields.passwordVariable(), AJS.I18n.getText("bcpd.config.dialog.login.password.error.format"));
                                    success = false;
                                }
                            }
                        }
                        else {
                            // Otherwise, the password field must have a value.
                            if (!this.fields.password().val()) {
                                addFieldError(this.fields.password(), AJS.I18n.getText("bcpd.config.dialog.login.password.error.required"));
                                success = false;
                            }
                        }
                    }
                    // If 'use atlassian id websudo' is selected, an appname must be provided
                    if (this.fields.useAtlassianIdWebSudo().prop("checked")) {
                        if (!this.fields.webSudoAppName().val()) {
                            addFieldError(this.fields.webSudoAppName(), AJS.I18n.getText("bcpd.config.dialog.websudo.appName.error.required"));
                            success = false;
                        }
                    }

                    return success;
                },

                disable: function () {
                    this.enabled = false;

                    // Disable the form
                    this.fields.webSudoAppName().prop("disabled", true);
                    $("span.icon-required", $("#bcpd-websudo-appname-label")).remove();
                    $("span.icon-required", $("#bcpd-websudo-password-label")).remove();

                    this.fields.useAtlassianIdWebSudo().prop("disabled", true);
                    this.fields.useProductWebSudo().prop("disabled", true);
                    this.fields.usePasswordVariable().prop("disabled", true);
                    this.fields.password().prop("disabled", true);
                    this.fields.passwordVariable().prop("disabled", true);

                    // Enable the warning message
                    $("#bcpd-websudo-not-required").show();
                },

                enable: function () {
                    this.enabled = true;

                    // Enable the form
                    this.fields.useAtlassianIdWebSudo().prop("disabled", false);
                    this.fields.useProductWebSudo().prop("disabled", false);

                    var useProductWebSudo = this.fields.useProductWebSudo().prop("checked");
                    this.fields.webSudoAppName().prop("disabled", useProductWebSudo);
                    if (useProductWebSudo) {
                        $("#bcpd-websudo-password-label").append("<span class=\"aui-icon icon-required\"></span>");
                    }

                    var useAtlassianIdWebSudo = this.fields.useAtlassianIdWebSudo().prop("checked");
                    this.fields.usePasswordVariable().prop("disabled", useAtlassianIdWebSudo);
                    this.fields.password().prop("disabled", useAtlassianIdWebSudo);
                    this.fields.passwordVariable().prop("disabled", useAtlassianIdWebSudo);
                    if (useAtlassianIdWebSudo) {
                        $("#bcpd-websudo-appname-label").append("<span class=\"aui-icon icon-required\"></span>");
                    }

                    // Hide the warning message
                    $("#bcpd-websudo-not-required").hide();
                },

                init: function () {
                    if (this.fields.useAtlassianIdWebSudo().prop("checked")) {
                        // Selecting product websudo disables the password fields
                        webSudoPanel.fields.usePasswordVariable().prop("disabled", true);
                        webSudoPanel.fields.password().prop("disabled", true);
                        webSudoPanel.fields.passwordVariable().prop("disabled", true);
                        $("span.icon-required", $("#bcpd-websudo-password-label")).remove();

                        // And enables the appname field (making it required)
                        webSudoPanel.fields.webSudoAppName().prop("disabled", false);
                        $("#bcpd-websudo-appname-label").append("<span class=\"aui-icon icon-required\"></span>");
                    } else {
                        // Selecting product websudo enables the password fields (making it required)
                        webSudoPanel.fields.usePasswordVariable().prop("disabled", false);
                        webSudoPanel.fields.password().prop("disabled", false);
                        webSudoPanel.fields.passwordVariable().prop("disabled", false);
                        $("#bcpd-websudo-password-label").append("<span class=\"aui-icon icon-required\"></span>");

                        // And disables the appname field
                        webSudoPanel.fields.webSudoAppName().prop("disabled", true);
                        $("span.icon-required", $("#bcpd-websudo-appname-label")).remove();
                    }

                    if (this.fields.usePasswordVariable().prop("checked")) {
                        webSudoPanel.fields.password().hide();
                        webSudoPanel.fields.passwordVariable().show();
                    } else {
                        webSudoPanel.fields.password().show();
                        webSudoPanel.fields.passwordVariable().hide();
                    }
                },

                wire: function (webSudoPanel$) {

                    // WebSudo Method toggles WebSudo Password text & AppName field state
                    this.fields.useAtlassianIdWebSudo().on("change", function () {
                        if ($(this).prop("checked")) {

                            // Selecting product websudo disables the password fields
                            webSudoPanel.fields.usePasswordVariable().prop("disabled", true);
                            webSudoPanel.fields.password().prop("disabled", true);
                            webSudoPanel.fields.passwordVariable().prop("disabled", true);
                            $("span.icon-required", $("#bcpd-websudo-password-label")).remove();

                            // And enables the appname field (making it required)
                            webSudoPanel.fields.webSudoAppName().prop("disabled", false);
                            $("#bcpd-websudo-appname-label").append("<span class=\"aui-icon icon-required\"></span>");
                        }
                    });

                    this.fields.useProductWebSudo().on("change", function () {
                        if ($(this).prop("checked")) {

                            // Selecting product websudo enables the password fields (making it required)
                            webSudoPanel.fields.usePasswordVariable().prop("disabled", false);
                            webSudoPanel.fields.password().prop("disabled", false);
                            webSudoPanel.fields.passwordVariable().prop("disabled", false);
                            $("#bcpd-websudo-password-label").append("<span class=\"aui-icon icon-required\"></span>");

                            // And disables the appname field
                            webSudoPanel.fields.webSudoAppName().prop("disabled", true);
                            $("span.icon-required", $("#bcpd-websudo-appname-label")).remove();
                        }
                    });

                    // Use Password Variable checkbox field toggles which password field is shown
                    this.fields.usePasswordVariable().on("change", function () {
                        if ($(this).prop("checked")) {
                            webSudoPanel.fields.password().hide();
                            webSudoPanel.fields.passwordVariable().show();
                        }
                        else {
                            webSudoPanel.fields.password().show();
                            webSudoPanel.fields.passwordVariable().hide();
                        }
                    });

                    // Bind all the inputs to clear their errors when focussed
                    $("input", webSudoPanel$).on("focus", function () {
                        var this$ = $(this);
                        removeFieldError(this$);
                    });
                },

                saveValues: function (values) {
                    if (!this.enabled)
                        return;

                    if (this.fields.useProductWebSudo().prop("checked")) {

                        values.useAtlassianIdWebSudo = false;
                        values.useWebSudoPasswordVariable = this.fields.usePasswordVariable().prop("checked") === true;
                        values.webSudoPassword = values.useWebSudoPasswordVariable ? this.fields.passwordVariable().val() : this.fields.password().val();
                    } else {
                        values.useAtlassianIdWebSudo = true;
                        values.webSudoAppName = this.fields.webSudoAppName().val();
                    }
                }
        };
        }
        else
        {
            webSudoPanel = {
                panelId: 1,
                validate: function() {
                    return true;
                },
                disable: function() {
                    // no-op;
                },
                enable: function() {
                    // no-op;
                },
                init: function() {
                    // no-op;
                },
                wire: function(webSudoPanel$) {
                    // no-op;
                },
                saveValues: function(values) {
                    // no-op;
                }


            };
        }

        var dialog = new AJS.Dialog({width: 750, height: 500, keypressListener: function(e) {
            // Need to override the different keypressListener, which just hides the dialog when ESC is pressed. We want
            // to actually _remove_ the dialog when ESC is pressed.
            if (e.keyCode === 27) {
                dialog.remove();
                dialog = null;
            }
        }});

        // Login credentials
        var values = getValues();
        dialog.addPanel(AJS.I18n.getText("bcpd.config.dialog.login.title"), confdeploy.loginPanel({url: $("#bcpd_config_baseUrl").val(), product: $("#bcpd_config_product").data("product-name"), values: values}), "bcpd-login-panel", loginPanel.panelId);
        var loginPanel$ = $(".bcpd-login-panel");

        var websudoPanelHtml;
        if (AJS.$("#bcpd_config_product").data("supports-websudo"))
        {
            websudoPanelHtml = confdeploy.websudoPanel({values: values, product: $("#bcpd_config_product").data("product-name")});
        }
        else
        {
            websudoPanelHtml = confdeploy.websudoBlankPanel({product: $("#bcpd_config_product").data("product-name")});
        }

        dialog.addPanel(AJS.I18n.getText("bcpd.config.dialog.websudo.title"), websudoPanelHtml, "bcpd-websudo-panel", webSudoPanel.panelId);
        var webSudoPanel$ = $(".bcpd-websudo-panel");

        dialog.addHeader(AJS.I18n.getText("bcpd.config.dialog.title"), "bcpd-dialog-title");
        dialog.addSubmit(AJS.I18n.getText("bcpd.config.dialog.save"), function() {
            // Validate
            // Clear all the existing errors
            $("div.error", loginPanel$).remove();
            $("div.error", webSudoPanel$).remove();

            var lpValidate = loginPanel.validate();
            var wspValidate = webSudoPanel.validate();

            if (!lpValidate)
            {
                dialog.gotoPanel(loginPanel.panelId);
                return;
            }
            else if (!wspValidate)
            {
                dialog.gotoPanel(webSudoPanel.panelId);
                return;
            }

            // Update deetz
            var values = {};
            loginPanel.saveValues(values);
            webSudoPanel.saveValues(values);
            putValues(values);

            // Close
            dialog.remove();
            dialog = null;
        });
        dialog.addCancel(AJS.I18n.getText("bcpd.config.dialog.cancel"), function() {
            // Close
            dialog.remove();
            dialog = null;
        });

        webSudoPanel.init();
        loginPanel.init();
        loginPanel.wire(loginPanel$);
        webSudoPanel.wire(webSudoPanel$);

        dialog.gotoPanel(0);
        dialog.show();

        event.preventDefault();
    });

})(AJS.$);
