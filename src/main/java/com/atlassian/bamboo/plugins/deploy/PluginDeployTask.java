package com.atlassian.bamboo.plugins.deploy;

import com.atlassian.bamboo.plugins.confdeploy.AutoDeployTask;
import com.atlassian.bamboo.plugins.confdeploy.config.RemoteProductType;
import com.atlassian.bamboo.plugins.confdeploy.config.TaskConfiguration;

/**
 * Implementation of {@link com.atlassian.bamboo.plugins.confdeploy.AutoDeployTask} where the target product type is
 * defined in the task configuration, rather than being defined by the task type itself.
 *a
 * This replaces the now deprecated "*ProductName*DeployTask" classes. In future, the deprecated tasks will be removed
 * and this will be the only task.
 */
public class PluginDeployTask extends AutoDeployTask
{
    @Override
    public RemoteProductType getProductType(TaskConfiguration configuration)
    {
        // Unlike other implementations where this value is hard-coded, this time we look up the configured product from
        // the task's configuration
        return configuration.getProduct().getOrElse(RemoteProductType.JIRA); // The 'else' case should never happen.
    }
}
