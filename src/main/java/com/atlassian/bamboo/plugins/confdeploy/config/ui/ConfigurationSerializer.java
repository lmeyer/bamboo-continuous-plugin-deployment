package com.atlassian.bamboo.plugins.confdeploy.config.ui;

import com.atlassian.bamboo.task.TaskDefinition;

import java.util.Map;

/**
 *
 */
public interface ConfigurationSerializer
{
    Map<String, String> serializeTaskContext(final TaskParametersMap params, final TaskDefinition taskDefinition);

    void deserializeTaskContext(final Map<String, Object> context, final TaskDefinition taskDefinition);
}
