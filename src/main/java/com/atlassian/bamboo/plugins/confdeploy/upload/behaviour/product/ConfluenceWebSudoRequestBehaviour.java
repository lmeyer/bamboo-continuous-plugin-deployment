package com.atlassian.bamboo.plugins.confdeploy.upload.behaviour.product;

import com.atlassian.bamboo.plugins.confdeploy.UrlUtils;
import com.atlassian.bamboo.plugins.confdeploy.upload.behaviour.ProductWebSudoRequestBehaviour;

/**
 * Use me for dancing with Confluence's WebSudo implementation.
 */
public class ConfluenceWebSudoRequestBehaviour extends ProductWebSudoRequestBehaviour
{
    public ConfluenceWebSudoRequestBehaviour(String baseUrl)
    {
        super(UrlUtils.join(baseUrl, "/doauthenticate.action"), "password");
    }
}
