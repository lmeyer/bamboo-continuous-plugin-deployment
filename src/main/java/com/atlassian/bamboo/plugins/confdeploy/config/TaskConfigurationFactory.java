package com.atlassian.bamboo.plugins.confdeploy.config;

import com.atlassian.bamboo.configuration.ConfigurationMap;
import com.atlassian.bamboo.plugins.confdeploy.AutoDeployTask;
import com.atlassian.bamboo.plugins.confdeploy.Failure;
import com.atlassian.bamboo.plugins.confdeploy.Result;
import com.atlassian.bamboo.task.CommonTaskContext;
import com.atlassian.fugue.Either;

/**
 * Encapsulates the logic required to re-create the configuration details of the {@link AutoDeployTask}
 * from a Bamboo task context (in both regular builds and deployment projects).
 */
public interface TaskConfigurationFactory
{
    /**
     * Infers the {@link TaskConfiguration} from the build's task context. If there are no configuration errors, a new
     * {@link TaskConfiguration} instance is returned. If the configuration could not be determined, a {@link Result}
     * instance is returned. If the {@link Result#isSuccess()} value is {@code false}, the task should not continue
     * execution.
     *
     * @param configurationMap The task's configuration parameters, obtained from the {@link CommonTaskContext}
     *
     * @return A {@link TaskConfiguration} for success, or a {@link Failure} for failure, wrapped in an {@link Either}.
     */
    public Either<TaskConfiguration, Failure> getTaskConfiguration(final ConfigurationMap configurationMap);
}
