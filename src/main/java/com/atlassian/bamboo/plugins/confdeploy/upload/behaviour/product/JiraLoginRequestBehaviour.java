package com.atlassian.bamboo.plugins.confdeploy.upload.behaviour.product;

import com.atlassian.bamboo.plugins.confdeploy.UrlUtils;
import com.atlassian.bamboo.plugins.confdeploy.upload.behaviour.ProductLoginRequestBehaviour;

/**
 * Use me for logging in to JIRA!
 */
public class JiraLoginRequestBehaviour extends ProductLoginRequestBehaviour
{
    public JiraLoginRequestBehaviour(final String baseUrl)
    {
        super(UrlUtils.join(baseUrl, "login.jsp"), "os_username", "os_password");
    }
}
