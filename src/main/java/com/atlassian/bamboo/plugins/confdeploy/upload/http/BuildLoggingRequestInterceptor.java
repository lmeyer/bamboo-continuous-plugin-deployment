package com.atlassian.bamboo.plugins.confdeploy.upload.http;

import com.atlassian.bamboo.build.logger.BuildLogger;
import com.atlassian.bamboo.plugins.confdeploy.config.TaskConfiguration;
import org.apache.commons.lang.StringUtils;
import org.apache.http.*;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;

import java.io.IOException;

/**
 * {@link HttpRequestInterceptor} that dumps {@link HttpRequest} information to the Bamboo build log.
 */
public class BuildLoggingRequestInterceptor implements HttpRequestInterceptor
{
    private final BuildLogger buildLogger;
    private final TaskConfiguration taskConfiguration;

    public BuildLoggingRequestInterceptor(final BuildLogger buildLogger, final TaskConfiguration taskConfiguration)
    {
        this.buildLogger = buildLogger;
        this.taskConfiguration = taskConfiguration;
    }

    @Override
    public void process(HttpRequest request, HttpContext context) throws HttpException, IOException
    {
        buildLogger.addBuildLogEntry("**** Outgoing Request Debug Log (" + context.getAttribute("http.target_host") + ") ****");
        buildLogger.addBuildLogEntry("> " + request.getRequestLine().toString());

        for (Header h : request.getAllHeaders())
        {
            logWithNoPasswords("> " + h.toString());
        }
        buildLogger.addBuildLogEntry("> ");

        if (request instanceof HttpEntityEnclosingRequest)
        {
            HttpEntity requestEntity = ((HttpEntityEnclosingRequest) request).getEntity();
            if (requestEntity.getContentType().getValue().startsWith("multipart/form-data"))
            {
                buildLogger.addBuildLogEntry("> (Multi-part Form Entity cannot be shown)");
            }
            else if (!requestEntity.isRepeatable())
            {
                requestEntity = new RepeatableHttpEntity(requestEntity);
                ((HttpEntityEnclosingRequest) request).setEntity(requestEntity);

                logWithNoPasswords("> " + EntityUtils.toString(requestEntity));
            }
            else
            {
                logWithNoPasswords("> " + EntityUtils.toString(requestEntity));
            }

        }
        buildLogger.addBuildLogEntry("************************************");
    }

    private void logWithNoPasswords(String logMessage)
    {
        if (StringUtils.isNotBlank(taskConfiguration.getPassword()) && logMessage.contains(taskConfiguration.getPassword()))
        {
            logMessage = logMessage.replace(taskConfiguration.getPassword(), "******");
        }

        if (StringUtils.isNotBlank(taskConfiguration.getAtlassianIdPassword()) && logMessage.contains(taskConfiguration.getAtlassianIdPassword()))
        {
            logMessage = logMessage.replace(taskConfiguration.getAtlassianIdPassword(), "******");
        }

        buildLogger.addBuildLogEntry(logMessage);
    }
}
