package com.atlassian.bamboo.plugins.confdeploy.upload.http;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.entity.HttpEntityWrapper;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Allows the {@link #getContent()} stream of the wrapped {@link HttpEntity} can be consumed more than once, by
 * buffering the entity's content into a local byte array.
 */
public class RepeatableHttpEntity extends HttpEntityWrapper
{
    private final ByteArrayInputStream cache;

    /**
     * Creates a new entity wrapper.
     *
     * @param wrapped the entity to wrap, not null
     * @throws IllegalArgumentException
     *          if wrapped is null
     * @throws IOException
     *          if the wrapped content stream cannot be read.
     */
    public RepeatableHttpEntity(HttpEntity wrapped) throws IOException
    {
        super(wrapped);

        // Prime the local buffer
        cache = initCache();
    }

    private ByteArrayInputStream initCache() throws IOException
    {

        InputStream content = super.getContent();
        try
        {
            ByteArrayOutputStream buffer = new ByteArrayOutputStream();
            IOUtils.copy(content, buffer);

            return new ByteArrayInputStream(buffer.toByteArray());

        }
        finally
        {
            content.close();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public InputStream getContent() throws IOException
    {
        // Seek the local buffer back to the beginning of the stream each time it is requested.
        cache.reset();

        return cache;
    }
}
