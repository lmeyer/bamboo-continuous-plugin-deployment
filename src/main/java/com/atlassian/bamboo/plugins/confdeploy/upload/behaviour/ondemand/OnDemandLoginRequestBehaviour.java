package com.atlassian.bamboo.plugins.confdeploy.upload.behaviour.ondemand;

import com.atlassian.bamboo.plugins.confdeploy.Result;
import com.atlassian.bamboo.plugins.confdeploy.UrlUtils;
import com.atlassian.bamboo.plugins.confdeploy.upload.behaviour.ProductLoginRequestBehaviour;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;

import java.io.IOException;

/**
 * Knows how to login to the Atlassian OnDemand shared login service, rather than the product-based login system.
 */
public class OnDemandLoginRequestBehaviour extends ProductLoginRequestBehaviour
{
    public OnDemandLoginRequestBehaviour(final String baseUrl)
    {
        // OnDemand login is a shared service available at /login from the root host URL. So for example, logging in
        // to https://ecosystem.atlassian.net/wiki should in fact POST to https://ecosystem.atlassian.net/login
        super(UrlUtils.join(UrlUtils.getHostUrl(baseUrl), "/login"), "username", "password");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Result handleResponse(HttpResponse response) throws IOException
    {
        try
        {
            int statusCode = response.getStatusLine().getStatusCode();
            if (statusCode == 302)
            {
                // Apparently redirects can happen when trying to login to FECRU in OnDemand, and another login request
                // to the supplied URL is required in order to "really" login (I guess this because FECRU doesn't support
                // the OnDemand shared login service properly?
                //
                // I could hot-wire Apache HTTPClient to follow the redirect, but since we're shutting down FECRU in
                // OnDemand in 2 weeks, it doesn't seem to be worth the effort.
                return Result.failure("OnDemand login returned HTTP 302 - are you trying to login to FECRU in OnDemand? This plugin doesn't support that yet, sorry!");
            }
            else if (statusCode == 200)
            {
                return Result.success("OnDemand login succeeded!");
            }
            else
            {
                return Result.failure("OnDemand login failed. HTTP Status code " + statusCode + " was returned.");
            }
        }
        finally
        {
            EntityUtils.consume(response.getEntity());
        }
    }
}
