package com.atlassian.bamboo.plugins.confdeploy;

import com.atlassian.fugue.Pair;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringUtils;

import javax.crypto.*;
import javax.crypto.spec.SecretKeySpec;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

/**
 * Handles encryption/decryption of the Confluence password.
 */
public class Crypto
{
    private static final String ALGORITHM = "AES";

    public static String decrypt(String key, String encrypted)
    {
        // Rebuild the key object from the supplied key string
        SecretKeySpec keySpec = new SecretKeySpec(Base64.decodeBase64(key), ALGORITHM);

        // Perform the decryption
        try
        {
            Cipher cipher = Cipher.getInstance(ALGORITHM);
            cipher.init(Cipher.DECRYPT_MODE, keySpec);

            return new String(cipher.doFinal(Base64.decodeBase64(encrypted)));
        }
        catch (InvalidKeyException e)
        {
            final String msg = String.format("Unable to decrypt value: %s", e.getMessage());
            throw new RuntimeException(msg, e);
        }
        catch (NoSuchAlgorithmException e)
        {
            final String msg = String.format("Unable to decrypt value: %s", e.getMessage());
            throw new RuntimeException(msg, e);
        }
        catch (NoSuchPaddingException e)
        {
            final String msg = String.format("Unable to decrypt value: %s", e.getMessage());
            throw new RuntimeException(msg, e);
        }
        catch (IllegalBlockSizeException e)
        {
            final String msg = String.format("Unable to decrypt value: %s", e.getMessage());
            throw new RuntimeException(msg, e);
        }
        catch (BadPaddingException e)
        {
            final String msg = String.format("Unable to decrypt value: %s", e.getMessage());
            throw new RuntimeException(msg, e);
        }
    }

    public static Pair<String, String> encrypt(String value)
    {
        if (StringUtils.isBlank(value))
            throw new RuntimeException("Cannot encrypt an empty value");

        // Rebuild the key object from the key string
        final String key = generateKey();
        SecretKeySpec keySpec = new SecretKeySpec(Base64.decodeBase64(key), ALGORITHM);

        Cipher cipher;
        try
        {
            cipher = Cipher.getInstance(ALGORITHM);
        }
        catch (NoSuchAlgorithmException e)
        {
            throw new RuntimeException(e);
        }
        catch (NoSuchPaddingException e)
        {
            throw new RuntimeException(e);
        }

        try
        {
            cipher.init(Cipher.ENCRYPT_MODE, keySpec);
        }
        catch (InvalidKeyException e)
        {
            throw new RuntimeException(e);
        }

        try
        {
            final String encrypted = Base64.encodeBase64String(cipher.doFinal(value.getBytes()));
            return new Pair<String, String>(key, encrypted);
        }
        catch (IllegalBlockSizeException e)
        {
            throw new RuntimeException(e);
        }
        catch (BadPaddingException e)
        {
            throw new RuntimeException(e);
        }
    }

    private static String generateKey()
    {
        final KeyGenerator keyGenerator;
        try
        {
            keyGenerator = KeyGenerator.getInstance(ALGORITHM);
        }
        catch (NoSuchAlgorithmException e)
        {
            final String msg = String.format("Unable to create KeyGenerator for %s algorithm: %s", ALGORITHM, e.getMessage());
            throw new RuntimeException(msg, e);
        }
        keyGenerator.init(128);

        SecretKey key = keyGenerator.generateKey();
        return Base64.encodeBase64String(key.getEncoded());
    }

}
