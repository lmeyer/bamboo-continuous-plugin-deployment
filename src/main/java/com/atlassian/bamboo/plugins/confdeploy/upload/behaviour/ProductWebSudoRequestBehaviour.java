package com.atlassian.bamboo.plugins.confdeploy.upload.behaviour;

import com.atlassian.bamboo.plugins.confdeploy.Failure;
import com.atlassian.bamboo.plugins.confdeploy.Result;
import com.atlassian.bamboo.plugins.confdeploy.config.TaskConfiguration;
import com.atlassian.fugue.Either;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Base class for WebSudo dancing with Atlassian products (for products that support it).
 */
public abstract class ProductWebSudoRequestBehaviour implements RequestBehaviour
{
    private static final String ENCODING = "utf-8";

    private final String webSudoUrl;
    private final String passwordFieldName;

    public ProductWebSudoRequestBehaviour(String webSudoUrl, String passwordFieldName)
    {
        this.webSudoUrl = webSudoUrl;
        this.passwordFieldName = passwordFieldName;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Either<HttpUriRequest, Failure> getRequest(final TaskConfiguration configuration, final Map<String, Object> requestContext)
    {
        List<BasicNameValuePair> values = new ArrayList<BasicNameValuePair>();
        values.add(new BasicNameValuePair(passwordFieldName, configuration.getPassword()));
        values.add(new BasicNameValuePair("authenticate", "Confirm"));

        HttpEntity entity;
        try
        {
            entity = new UrlEncodedFormEntity(values, ENCODING);
        }
        catch (UnsupportedEncodingException e)
        {
            return Either.right(Result.failure("Failed to encode UTF-8 String - Java, you suck.", e));
        }

        HttpPost webSudoPost = new HttpPost(webSudoUrl);
        webSudoPost.addHeader("Accept", "*");
        webSudoPost.addHeader("X-Atlassian-Token", "no-check");
        webSudoPost.setEntity(entity);

        return Either.<HttpUriRequest, Failure>left(webSudoPost);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Result handleResponse(HttpResponse response) throws IOException
    {
        try
        {
            Header[] webSudoHeaders = response.getHeaders("X-Atlassian-WebSudo");
            if (webSudoHeaders == null || webSudoHeaders.length != 1)
            {
                return Result.failure("WebSudo authentication failed. A required header ('X-Atlassian-WebSudo') was missing from the response.");
            }
            String webSudoHeader = webSudoHeaders[0].getValue();
            if (!webSudoHeader.equals("Has-Authentication"))
            {
                return Result.failure("WebSudo authentication failed. The 'X-Atlassian-WebSudo' response header did not contain the expected value ('Has-Authentication').");
            }

            return Result.success("WebSudo authentication successful!");
        }
        finally
        {
            EntityUtils.consume(response.getEntity());
        }
    }
}
