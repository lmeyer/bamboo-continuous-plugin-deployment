package com.atlassian.bamboo.plugins.confdeploy.upload.behaviour.product;

import com.atlassian.bamboo.plugins.confdeploy.UrlUtils;
import com.atlassian.bamboo.plugins.confdeploy.upload.behaviour.ProductWebSudoRequestBehaviour;

/**
 * Use me for dancing with JIRA's WebSudo implementation.
 */
public class JiraWebSudoRequestBehaviour extends ProductWebSudoRequestBehaviour
{
    public JiraWebSudoRequestBehaviour(String baseUrl)
    {
        super(UrlUtils.join(baseUrl, "secure/admin/WebSudoAuthenticate.jspa"), "webSudoPassword");
    }
}
