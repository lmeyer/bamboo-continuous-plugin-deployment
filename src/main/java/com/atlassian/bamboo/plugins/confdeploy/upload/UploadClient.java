package com.atlassian.bamboo.plugins.confdeploy.upload;

import com.atlassian.bamboo.plugins.confdeploy.Result;
import com.atlassian.bamboo.plugins.confdeploy.config.TaskConfiguration;
import com.atlassian.bamboo.task.TaskException;

/**
 * Responsible for orchestrating the upload of the plugin artifact to the remote application. This involves authenticating
 * with the remote application, physically transferring the artifact and then checking to ensure that the new plugin
 * version was installed successfully.
 */
public interface UploadClient
{
    public Result deploy(final TaskConfiguration taskConfiguration) throws TaskException;
}
