package com.atlassian.bamboo.plugins.confdeploy.upload.behaviour.upm;

import com.atlassian.bamboo.plugins.confdeploy.Failure;
import com.atlassian.bamboo.plugins.confdeploy.Result;
import com.atlassian.bamboo.plugins.confdeploy.UrlUtils;
import com.atlassian.bamboo.plugins.confdeploy.config.TaskConfiguration;
import com.atlassian.bamboo.plugins.confdeploy.upload.behaviour.PollingRequestBehaviour;
import com.atlassian.fugue.Either;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Map;
import java.util.Random;

/**
 * Polls the UPM REST API to ensure that the uploaded plugin was actually installed successfully.
 */
public class UploadSuccessCheckBehaviour implements PollingRequestBehaviour
{
    private static final Random RAND = new Random();
    private final String hostUrl;

    public UploadSuccessCheckBehaviour(String baseUrl)
    {
        // The "self" link in the JSON response will include any path component of the Base URL. Need to strip
        // this out of the configured Base URL when concatenating them or else the context path of the app will
        // get duplicated.
        this.hostUrl = UrlUtils.getHostUrl(baseUrl);
    }

    /**
     * {@inheritDoc}
     */
    public Either<HttpUriRequest, Failure> getRequest(final TaskConfiguration configuration, final Map<String, Object> requestContext)
    {
        if (!requestContext.containsKey("responseEntity"))
        {
            return Either.right(Result.failure("Could not check plugin install status - the JSON Response entity is missing (probably a bug!)"));
        }
        JSONObject responseEntity = (JSONObject) requestContext.get("responseEntity");

        return getRequestInternal(responseEntity);
    }

    @Override
    public Either<HttpUriRequest, Result> apply(HttpResponse response)
    {
        try
        {
            try
            {
                int statusCode = response.getStatusLine().getStatusCode();
                if (statusCode == 200)
                {
                    String entity = EntityUtils.toString(response.getEntity());

                    JSONObject j = new JSONObject(entity);
                    if (!j.has("pingAfter"))
                    {
                        // No more polling is needed if there is no ping after value.
                        return Either.<HttpUriRequest, Result>right(Result.success("Plugin installed successfully"));
                    }

                    // Continue polling.
                    Thread.sleep(j.getInt("pingAfter")); // Wait the requested amount of time before checking again.

                    @SuppressWarnings("unchecked") // Is there a better way? Does java support co-variance?
                    Either<HttpUriRequest, Result> request = (Either<HttpUriRequest, Result>) (Object) getRequestInternal(j);
                    return request;
                }
                else if (statusCode >= 400)
                {
                    // Client failure. Stop polling & abort.
                    return Either.<HttpUriRequest, Result>right(Result.failure("Unexpected response code when polling upload: " + statusCode));
                }
                else
                {
                    // Any other response means polling finished successfully... I think.
                    return Either.<HttpUriRequest, Result>right(Result.success("Plugin installed successfully!"));
                }
            }
            finally
            {
                EntityUtils.consume(response.getEntity());
            }
        }
        catch (IOException e)
        {
            return Either.<HttpUriRequest, Result>right(Result.failure(e.getMessage(), e));
        }
        catch (JSONException e)
        {
            return Either.<HttpUriRequest, Result>right(Result.failure(e.getMessage(), e));
        }
        catch (InterruptedException e)
        {
            return Either.<HttpUriRequest, Result>right(Result.failure(e.getMessage(), e));
        }
    }

    private Either<HttpUriRequest, Failure> getRequestInternal(JSONObject responseEntity)
    {
        String requestUrl;
        try
        {
            requestUrl = UrlUtils.join(hostUrl, responseEntity.getJSONObject("links").getString("self") + "?_=" + RAND.nextLong());
        }
        catch (JSONException e)
        {
            return Either.right(Result.failure("Failed to get the polling link from the JSON response.", e));
        }

        HttpUriRequest request = new HttpGet(requestUrl);
        request.addHeader("Accept", "*/*");

        return Either.left(request);
    }
}
