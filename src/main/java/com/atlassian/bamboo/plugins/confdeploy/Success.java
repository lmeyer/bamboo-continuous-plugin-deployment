package com.atlassian.bamboo.plugins.confdeploy;

import javax.annotation.Nullable;

/**
 *
 */
public final class Success extends Result
{
    Success(String message)
    {
        super(message);
    }

    Success(String message, @Nullable Object context)
    {
        super(message, context);
    }

    @Override
    public boolean isSuccess()
    {
        return true;
    }
}
