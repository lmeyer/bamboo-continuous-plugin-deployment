package com.atlassian.bamboo.plugins.confdeploy.config.ui;

/**
 * Represents an {@link com.atlassian.bamboo.plan.artifact.ArtifactSubscription} available for use by an instance of
 * {@link com.atlassian.bamboo.plugins.confdeploy.AutoDeployTask}
 */
public class SubscribedArtifact
{
    private final String id;
    private final String group;
    private final String displayName;

    public SubscribedArtifact(final String id, final String group, final String displayName)
    {
        this.id = id;
        this.group = group;
        this.displayName = displayName;
    }

    public String getId()
    {
        return id;
    }

    public String getGroup()
    {
        return group;
    }

    public String getDisplayName()
    {
        return displayName;
    }
}
