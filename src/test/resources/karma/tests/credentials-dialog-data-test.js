/**
 * Test module for loading existing values into the 'edit credentials' dialog and making sure it behaves properly.
 */
define(['edit-task'], function(confDeploy) {

    module("Test Credentials Dialog - load existing values", {
        setup: function() {
            // Render the HTML fixture
            AJS.$("body").append(window.__html__['src/test/resources/karma/fixtures/blank-form.html']);
        },
        teardown: function() {
            AJS.$("body").empty();
        }
    });

    test("Load Bamboo product login values", function() {

        // Pre-populate the parent form with the persisted values.
        AJS.$("#bcpd_config_product").data("product-name", "Bamboo");
        AJS.$("#bcpd_config_baseUrl").val("http://localhost:6990/bamboo");
        AJS.$("#bcpd_config_username").val("sysadmin");
        AJS.$("#bcpd_config_password").val("397ef670-6140-11e3-949a-0800200c9a66");
        AJS.$("#bcpd_config_usePasswordVariable").val("false");
        AJS.$("#bcpd_config_useAtlassianId").val("false");

        // Load the dialog
        AJS.$("#bcpd_config_credentials").click();

        // Ensure that the values were loaded into the dialog correctly
        var username = AJS.$("#bcpd-login-username");
        equal(username.val(), "sysadmin");

        var password = AJS.$("#bcpd-login-password");
        equal(password.val(), "397ef670-6140-11e3-949a-0800200c9a66");

        var useProductLogin = AJS.$("#bcpd-login-useAtlassianId-productLogin");
        ok(useProductLogin.prop("checked"));
        var useAtlassianId = AJS.$("#bcpd-login-useAtlassianId-atlassianId");
        ok(!useAtlassianId.prop("checked"));

        // Ensure websudo panel is disabled
        AJS.$("form#bcpd-websudo-config input").each(function() {
            ok($(this).is(":disabled"));
        });
        // The websudo panel displays a message explaining why the form is disabled
        AJS.$("button:contains('bcpd.config.dialog.websudo.title')").click();
        ok(AJS.$("#bcpd-websudo-not-required").is(":visible"));
    });

    test("Load Confluence product login values with password variable", function() {

        // Pre-populate the parent form with the persisted values.
        AJS.$("#bcpd_config_product").data("product-name", "Confluence");
        AJS.$("#bcpd_config_baseUrl").val("https://pug.jira.com/wiki");
        AJS.$("#bcpd_config_username").val("sysadmin");
        AJS.$("#bcpd_config_password").val("");
        AJS.$("#bcpd_config_usePasswordVariable").val("true");
        AJS.$("#bcpd_config_passwordVariable").val("${bamboo.mypassword}");
        AJS.$("#bcpd_config_useAtlassianId").val("false");

        // Load the dialog
        AJS.$("#bcpd_config_credentials").click();

        // Ensure that the values were loaded into the dialog correctly.
        var username = AJS.$("#bcpd-login-username");
        equal(username.val(), "sysadmin");

        var password = AJS.$("#bcpd-login-password");
        ok(password.is(":hidden"));
        equal(password.val(), "");

        var usePasswordVariable = AJS.$("#bcpd-login-usePasswordVariable");
        ok(usePasswordVariable.is(":enabled"));
        ok(usePasswordVariable.prop("checked"));

        var passwordVariable = AJS.$("#bcpd-login-passwordVariable");
        ok(passwordVariable.is(":visible"));
        equal(passwordVariable.val(), "${bamboo.mypassword}");

        var atlassianIdBaseUrl = AJS.$("#bcpd-login-atlassianIdBaseUrl");
        ok(atlassianIdBaseUrl.is(":disabled"));
        equal(atlassianIdBaseUrl.val(), "");

        // Ensure websudo panel is disabled
        AJS.$("form#bcpd-websudo-config input").each(function() {
            ok($(this).is(":disabled"));
        });
        // The websudo panel displays a message explaining why the form is disabled
        AJS.$("button:contains('bcpd.config.dialog.websudo.title')").click();
        ok(AJS.$("#bcpd-websudo-not-required").is(":visible"));
    });

    test("Load Confluence with Atlassian ID login and Atlassian ID WebSudo", function() {

        // Pre-populate the parent form with the persisted values.
        AJS.$("#bcpd_config_product").data("product-name", "Confluence");
        AJS.$("#bcpd_config_baseUrl").val("https://confluence.atlassian.com");
        AJS.$("#bcpd_config_username").val("");
        AJS.$("#bcpd_config_password").val("");
        AJS.$("#bcpd_config_usePasswordVariable").val("false");

        AJS.$("#bcpd_config_useAtlassianId").val("true");
        AJS.$("#bcpd_config_atlassianIdBaseURL").val("https://id.atlassian.com");
        AJS.$("#bcpd_config_atlassianIdUsername").val("sysadmin@mydomain.com");
        AJS.$("#bcpd_config_atlassianIdPassword").val("397ef670-6140-11e3-949a-0800200c9a66");
        AJS.$("#bcpd_config_useAtlassianIdPasswordVariable").val("false");
        AJS.$("#bcpd_config_useAtlassianIdWebSudo").val("true");
        AJS.$("#bcpd_config_atlassianIdAppName").val("cac");

        // Load the dialog
        AJS.$("#bcpd_config_credentials").click();

        // Ensure that the values were loaded into the dialog correctly.
        var useProductLogin = AJS.$("#bcpd-login-useAtlassianId-productLogin");
        ok(!useProductLogin.prop("checked"));
        var useAtlassianId = AJS.$("#bcpd-login-useAtlassianId-atlassianId");
        ok(useAtlassianId.prop("checked"));

        var username = AJS.$("#bcpd-login-username");
        equal(username.val(), "sysadmin@mydomain.com");

        var password = AJS.$("#bcpd-login-password");
        equal(password.val(), "397ef670-6140-11e3-949a-0800200c9a66");

        var atlassianIdBaseUrl = AJS.$("#bcpd-login-atlassianIdBaseUrl");
        ok(atlassianIdBaseUrl.is(":enabled"));
        equal(atlassianIdBaseUrl.val(), "https://id.atlassian.com");

        var useAtlassianIdWebSudo = AJS.$("#bcpd-websudo-method-lasso");
        ok(useAtlassianIdWebSudo.prop("checked"));
        var useProductWebSudo = AJS.$("#bcpd-websudo-method-product");
        ok(!useProductWebSudo.prop("checked"));

        var webSudoPassword = AJS.$("#bcpd-websudo-password");
        ok(webSudoPassword.is(":disabled"));
        equal(webSudoPassword.val(), "");

        var webSudoAppName = AJS.$("#bcpd-websudo-appname");
        ok(webSudoAppName.is(":enabled"));
        equal(webSudoAppName.val(), "cac");
    });

    test("Load JIRA with Atlassian ID login and Atlassian ID WebSudo with password variable", function() {
        // Pre-populate the parent form with the persisted values.
        AJS.$("#bcpd_config_product").data("product-name", "JIRA");
        AJS.$("#bcpd_config_baseUrl").val("https://jira.atlassian.com");
        AJS.$("#bcpd_config_username").val("");
        AJS.$("#bcpd_config_password").val("");
        AJS.$("#bcpd_config_usePasswordVariable").val("false");

        AJS.$("#bcpd_config_useAtlassianId").val("true");
        AJS.$("#bcpd_config_atlassianIdBaseURL").val("https://id.atlassian.com");
        AJS.$("#bcpd_config_atlassianIdUsername").val("sysadmin@mydomain.com");
        AJS.$("#bcpd_config_atlassianIdPassword").val("");
        AJS.$("#bcpd_config_useAtlassianIdPasswordVariable").val("true");
        AJS.$("#bcpd_config_atlassianIdPasswordVariable").val("${bamboo.lasso.password}");
        AJS.$("#bcpd_config_useAtlassianIdWebSudo").val("true");
        AJS.$("#bcpd_config_atlassianIdAppName").val("jac");

        // Load the dialog
        AJS.$("#bcpd_config_credentials").click();

        // Ensure that the values were loaded into the dialog correctly.
        var useProductLogin = AJS.$("#bcpd-login-useAtlassianId-productLogin");
        ok(!useProductLogin.prop("checked"));
        var useAtlassianId = AJS.$("#bcpd-login-useAtlassianId-atlassianId");
        ok(useAtlassianId.prop("checked"));

        var username = AJS.$("#bcpd-login-username");
        equal(username.val(), "sysadmin@mydomain.com");

        var password = AJS.$("#bcpd-login-password");
        ok(password.is(":hidden"));
        equal(password.val(), "");

        var usePasswordVariable = AJS.$("#bcpd-login-usePasswordVariable");
        ok(usePasswordVariable.prop("checked"));

        var passwordVariable = AJS.$("#bcpd-login-passwordVariable");
        ok(passwordVariable.is(":visible"));
        equal(passwordVariable.val(), "${bamboo.lasso.password}");

        var atlassianIdBaseUrl = AJS.$("#bcpd-login-atlassianIdBaseUrl");
        ok(atlassianIdBaseUrl.is(":enabled"));
        equal(atlassianIdBaseUrl.val(), "https://id.atlassian.com");

        var useAtlassianIdWebSudo = AJS.$("#bcpd-websudo-method-lasso");
        ok(useAtlassianIdWebSudo.prop("checked"));
        var useProductWebSudo = AJS.$("#bcpd-websudo-method-product");
        ok(!useProductWebSudo.prop("checked"));

        var webSudoPassword = AJS.$("#bcpd-websudo-password");
        ok(webSudoPassword.is(":disabled"));
        equal(webSudoPassword.val(), "");

        var webSudoAppName = AJS.$("#bcpd-websudo-appname");
        ok(webSudoAppName.is(":enabled"));
        equal(webSudoAppName.val(), "jac");
    });

    test("Load JIRA with Atlassian ID login and product-based WebSudo", function() {

        // Pre-populate the parent form with the persisted values.
        AJS.$("#bcpd_config_product").data("product-name", "JIRA");
        AJS.$("#bcpd_config_baseUrl").val("https://jira.stg.internal.atlassian.com");
        AJS.$("#bcpd_config_username").val("");
        AJS.$("#bcpd_config_usePasswordVariable").val("false");

        AJS.$("#bcpd_config_useAtlassianId").val("true");
        AJS.$("#bcpd_config_atlassianIdBaseURL").val("https://id.stg.iam.atlassian.com");
        AJS.$("#bcpd_config_atlassianIdUsername").val("sysadmin@mydomain.com");
        AJS.$("#bcpd_config_atlassianIdPassword").val("397ef670-6140-11e3-949a-0800200c9a66");
        AJS.$("#bcpd_config_useAtlassianIdPasswordVariable").val("false");

        AJS.$("#bcpd_config_useAtlassianIdWebSudo").val("false");
        AJS.$("#bcpd_config_atlassianIdAppName").val("");
        AJS.$("#bcpd_config_password").val("397ef670-6140-11e3-949a-0800200c9a66");

        // Load the dialog
        AJS.$("#bcpd_config_credentials").click();

        // Ensure that the values were loaded into the dialog correctly.
        var useProductLogin = AJS.$("#bcpd-login-useAtlassianId-productLogin");
        ok(!useProductLogin.prop("checked"));
        var useAtlassianId = AJS.$("#bcpd-login-useAtlassianId-atlassianId");
        ok(useAtlassianId.prop("checked"));

        var username = AJS.$("#bcpd-login-username");
        equal(username.val(), "sysadmin@mydomain.com");

        var password = AJS.$("#bcpd-login-password");
        equal(password.val(), "397ef670-6140-11e3-949a-0800200c9a66");

        var atlassianIdBaseUrl = AJS.$("#bcpd-login-atlassianIdBaseUrl");
        ok(atlassianIdBaseUrl.is(":enabled"));
        equal(atlassianIdBaseUrl.val(), "https://id.stg.iam.atlassian.com");

        var useAtlassianIdWebSudo = AJS.$("#bcpd-websudo-method-lasso");
        ok(!useAtlassianIdWebSudo.prop("checked"));
        var useProductWebSudo = AJS.$("#bcpd-websudo-method-product");
        ok(useProductWebSudo.prop("checked"));

        var webSudoPassword = AJS.$("#bcpd-websudo-password");
        equal(webSudoPassword.val(), "397ef670-6140-11e3-949a-0800200c9a66");

        var webSudoAppName = AJS.$("#bcpd-websudo-appname");
        ok(webSudoAppName.is(":disabled"));
        equal(webSudoAppName.val(), "");
    });
});
