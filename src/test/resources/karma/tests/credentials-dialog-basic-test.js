/**
 * Test module for the basic functionality of the "Edit Credentials" dialog.
 */
define(['edit-task'], function (confDeploy) {

    module("Test Credentials Dialog - Basic Tests", {
        setup: function () {
            // Render the HTML fixture
            AJS.$("body").append(window.__html__['src/test/resources/karma/fixtures/blank-form.html']);
        },
        teardown: function() {
            AJS.$("body").empty();
        }
    });

    test("Clicking 'Edit Credentials' opens the dialog", function() {

        // Dialog not present
        var dialog = AJS.$("h2.bcpd-dialog-title");
        equal(dialog.length, 0, "dialog was present before the button was clicked");

        // Click the 'Edit Credentials' button to load the dialog.
        AJS.$("#bcpd_config_credentials").click();

        dialog = AJS.$("h2.bcpd-dialog-title");
        equal(dialog.length, 1, "expected a single dialog instance after clicking the button");
    });

    test("Clicking 'Cancel' closes the dialog", function() {

        // Dialog initially not present
        var dialog = AJS.$("h2.bcpd-dialog-title");
        equal(dialog.length, 0, "dialog was present before the button was clicked");

        // Click the 'Edit Credentials' button to load the dialog.
        AJS.$("#bcpd_config_credentials").click();

        dialog = AJS.$("h2.bcpd-dialog-title");
        equal(dialog.length, 1, "expected a single dialog instance after clicking the button");

        // Click the 'cancel' link
        var cancel = AJS.$(".button-panel-cancel-link");
        equal(cancel.length, 1, "Couldn't locate the 'Cancel' link on the dialog");

        cancel.click();

        // Dialog should now be gone!
        dialog = AJS.$("h2.bcpd-dialog-title");
        equal(dialog.length, 0, "Dialog was not destroyed after 'Cancel' was clicked.");
    });

    test("Product and URL populated in dialog", function() {

        // Specify the product type and Base URL in the parent Form.
        AJS.$("#bcpd_config_product").data("product-name", "Atlassian Product Name");
        AJS.$("#bcpd_config_baseUrl").val("http://karma-runner.github.io");

        // Load the dialog
        AJS.$("#bcpd_config_credentials").click();

        // The entered values should have been substituted into the dialog text.
        var productLoginText = AJS.$("label[for=\"bcpd-login-useAtlassianId-productLogin\"]").text();
        equal(productLoginText, "Use Atlassian Product Name login");

        var urlLink = AJS.$("a[href=\"http://karma-runner.github.io\"]", AJS.$(".bcpd-login-panel"));
        equal(urlLink.length, 1, "Couldn't find link to Base URL in dialog login panel");
    });

    test("WebSudo panel empty for products that don't need it", function() {
        // switch off websudo
        AJS.$("#bcpd_config_product").data("supports-websudo", false);

        // Load the dialog
        AJS.$("#bcpd_config_credentials").click();

        // Click on the websudo panel and make sure it's empty save for a warning message.
        AJS.$("button:contains('bcpd.config.dialog.websudo.title')").click();
        equal(AJS.$("#bcpd-websudo-config").length, 0);
        equal(AJS.$("#bcpd-websudo-not-required").length, 1);
    });

    test("Test correct default state for blank dialog", function() {
        // Load the dialog
        AJS.$("#bcpd_config_credentials").click();

        // Ensure 'product login' radio button is selected
        ok(AJS.$("#bcpd-login-useAtlassianId-productLogin").prop("checked"), "Use Product Login should have been selected");
        // (ergo, 'atlassian ID' radio button is not selected)
        ok(!AJS.$("#bcpd-login-useAtlassianId-atlassianId").prop("checked"), "Use Atlassian ID login should not be selected");

        // The login panel should be selected and visible by default
        ok(AJS.$(".bcpd-login-panel").is(":visible"));
        // (ergo the websudo panel should be hidden)
        ok(AJS.$(".bcpd-websudo-panel").is(":hidden"));

        // The username & password fields are visible, enabled and blank
        var username = AJS.$("#bcpd-login-username");
        ok(username.is(":visible"));
        ok(username.is(":enabled"));
        equal(username.val(), "");

        var password = AJS.$("#bcpd-login-password");
        ok(password.is(":visible"));
        ok(password.is(":enabled"));
        equal(password.val(), "");

        // The password variable checkbox is visible, enabled and un-checked
        var usePasswordVariableCheckbox = AJS.$("#bcpd-login-usePasswordVariable");
        ok(usePasswordVariableCheckbox.is(":visible"));
        ok(usePasswordVariableCheckbox.is(":enabled"));
        equal(usePasswordVariableCheckbox.prop("checked"), false);

        // The password variable input field is hidden, enabled and blank.
        var usePasswordVariable = AJS.$("#bcpd-login-passwordVariable");
        ok(usePasswordVariable.is(":hidden"));
        ok(usePasswordVariable.is(":enabled"));
        equal(usePasswordVariable.val(), "");

        // The Atlassian ID Base URL input field is visibled, _disabled_ and blank..
        var atlassianIdBaseUrl = AJS.$("#bcpd-login-atlassianIdBaseUrl");
        ok(atlassianIdBaseUrl.is(":visible"));
        ok(atlassianIdBaseUrl.is(":disabled"));
        equal(atlassianIdBaseUrl.val(), "");

        // The WebSudo options form should be completely disabled (as the product login option is selected)
        AJS.$("form#bcpd-websudo-config input").each(function() {
            ok($(this).is(":disabled"));
        });

        // The websudo panel displays a message explaining why the form is disabled
        AJS.$("button:contains('bcpd.config.dialog.websudo.title')").click();
        ok(AJS.$("#bcpd-websudo-not-required").is(":visible"));
    });

    test("Test dialog can be loaded multiple times", function() {

        for (var i = 0; i < 10; i++)
        {
            // Dialog initially not present
            var dialog = AJS.$("h2.bcpd-dialog-title");
            equal(dialog.length, 0, "dialog was present before the button was clicked");

            // Click the 'Edit Credentials' button to load the dialog.
            AJS.$("#bcpd_config_credentials").click();

            dialog = AJS.$("h2.bcpd-dialog-title");
            equal(dialog.length, 1, "expected a single dialog instance after clicking the button");

            // Click the 'cancel' link
            var cancel = AJS.$(".button-panel-cancel-link");
            equal(cancel.length, 1, "Couldn't locate the 'Cancel' link on the dialog");

            cancel.click();

            // Dialog should now be gone!
            dialog = AJS.$("h2.bcpd-dialog-title");
            equal(dialog.length, 0, "Dialog was not destroyed after 'Cancel' was clicked.");
        }
    });
});