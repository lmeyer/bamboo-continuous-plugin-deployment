package testsupport.assertions;

import org.apache.http.client.methods.HttpUriRequest;

/**
 * A single test assertion that acts on a {@link HttpUriRequest}.
 */
public interface HttpUriRequestAssertion
{
    public void assertRequest(HttpUriRequest request);
}
