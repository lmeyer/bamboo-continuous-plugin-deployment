package testsupport.assertions;

import com.google.common.base.Predicate;
import org.apache.http.HttpEntity;
import org.apache.http.HttpEntityEnclosingRequest;
import org.apache.http.NameValuePair;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.utils.URLEncodedUtils;

import java.io.IOException;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Asserts that a {@link HttpUriRequest} body is application/x-www-form-urlencoded and contains some specific content.
 */
public class FormEncodedRequestBodyAssertion implements HttpUriRequestAssertion
{
    private final Predicate<List<NameValuePair>> bodyPredicate;

    public FormEncodedRequestBodyAssertion(final List<NameValuePair> expected)
    {
        this.bodyPredicate = new Predicate<List<NameValuePair>>()
        {
            @Override
            public boolean apply(List<NameValuePair> actual)
            {
                assertEquals(expected.size(), actual.size());
                for (NameValuePair nvp : expected)
                {
                    assertTrue(actual.contains(nvp));
                }
                return true;
            }
        };
    }

    @Override
    public void assertRequest(HttpUriRequest request)
    {
        HttpEntity entity = ((HttpEntityEnclosingRequest) request).getEntity();
        List<NameValuePair> requestParameters = null;
        try
        {
           requestParameters = URLEncodedUtils.parse(entity);
        }
        catch (IOException e)
        {
            fail(e.toString());
        }

        bodyPredicate.apply(requestParameters);
    }
}
