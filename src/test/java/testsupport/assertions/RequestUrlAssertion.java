package testsupport.assertions;

import org.apache.http.client.methods.HttpUriRequest;
import org.junit.Assert;

/**
 * Asserts that a {@link HttpUriRequest} has the expected Request URL.
 */
public class RequestUrlAssertion implements HttpUriRequestAssertion
{
    private final String expectedUrl;

    public RequestUrlAssertion(final String expectedUrl)
    {
        this.expectedUrl = expectedUrl;
    }

    @Override
    public void assertRequest(HttpUriRequest request)
    {
        Assert.assertEquals(expectedUrl, request.getURI().toASCIIString());
    }
}
