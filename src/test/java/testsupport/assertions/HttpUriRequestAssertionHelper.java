package testsupport.assertions;

import org.apache.http.client.methods.HttpUriRequest;

import java.util.List;

/**
 * Helper class for asserting on the state of a {@link HttpUriRequest} instance.
 */
public class HttpUriRequestAssertionHelper
{
    public static void assertRequest(final HttpUriRequest request, final List<HttpUriRequestAssertion> assertions)
    {
        for (HttpUriRequestAssertion assertion : assertions)
        {
            assertion.assertRequest(request);
        }
    }
}
