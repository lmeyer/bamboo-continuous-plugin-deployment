package com.atlassian.bamboo.plugins.confdeploy.upload.behaviour.product;

import com.atlassian.bamboo.plugins.confdeploy.Failure;
import com.atlassian.bamboo.plugins.confdeploy.Result;
import com.atlassian.bamboo.plugins.confdeploy.config.TaskConfiguration;
import com.atlassian.bamboo.plugins.confdeploy.upload.behaviour.RequestBehaviour;
import com.atlassian.fugue.Either;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.StringEntity;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import testsupport.MockTaskConfigurationFactory;

import java.io.InputStream;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static testsupport.MockTaskConfigurationFactory.BASE_URL;

/**
 * Test harness for {@link JiraPermissionCheckBehaviour}.
 */
public class TestJiraPermissionCheckBehaviour
{
    private static String RESPONSE_HAS_PERMISSION;
    private static String RESPONSE_NO_PERMISSION;

    private RequestBehaviour behaviour;

    @BeforeClass
    public static void loadResources() throws Exception
    {
        final ClassLoader myClassLoader = TestJiraPermissionCheckBehaviour.class.getClassLoader();

        InputStream resourceStream = null;
        try
        {
            resourceStream = myClassLoader.getResourceAsStream("tests/TestJiraPermissionCheckBehaviour-haspermission.json");
            RESPONSE_HAS_PERMISSION = IOUtils.toString(resourceStream);
        }
        finally
        {
            if (resourceStream != null)
                IOUtils.closeQuietly(resourceStream);
        }

        resourceStream = null;
        try
        {
            resourceStream = myClassLoader.getResourceAsStream("tests/TestJiraPermissionCheckBehaviour-nopermission.json");
            RESPONSE_NO_PERMISSION = IOUtils.toString(resourceStream);
        }
        finally
        {
            if (resourceStream != null)
                IOUtils.closeQuietly(resourceStream);
        }
    }

    @Before
    public void setUp()
    {
        behaviour = new JiraPermissionCheckBehaviour();

    }

    @Test
    public void testGetRequest()
    {
        TaskConfiguration configuration = MockTaskConfigurationFactory.getMockTaskConfiguration(ImmutableMap.<String, Object>of(BASE_URL, "https://jira.atlassian.com"));

        Either<HttpUriRequest,Failure> maybeRequest = behaviour.getRequest(configuration, Maps.<String, Object>newHashMap());
        assertTrue(maybeRequest.isLeft());

        HttpUriRequest request = maybeRequest.left().get();
        assertTrue(request instanceof HttpGet);
        assertEquals("https://jira.atlassian.com/rest/api/2/mypermissions", request.getURI().toASCIIString());
    }

    @Test
    public void testResponseHasPermission() throws Exception
    {
        StatusLine statusLine = mock(StatusLine.class);
        when(statusLine.getStatusCode()).thenReturn(200);
        HttpResponse response = mock(HttpResponse.class);
        when(response.getStatusLine()).thenReturn(statusLine);
        when(response.getEntity()).thenReturn(new StringEntity(RESPONSE_HAS_PERMISSION));

        Result result = behaviour.handleResponse(response);
        assertTrue(result.isSuccess());
    }

    @Test
    public void testResponseNoPermission() throws Exception
    {
        StatusLine statusLine = mock(StatusLine.class);
        when(statusLine.getStatusCode()).thenReturn(200);
        HttpResponse response = mock(HttpResponse.class);
        when(response.getStatusLine()).thenReturn(statusLine);
        when(response.getEntity()).thenReturn(new StringEntity(RESPONSE_NO_PERMISSION));

        Result result = behaviour.handleResponse(response);
        assertFalse(result.isSuccess());
        assertEquals("The user configured for this task does not have system administration privileges on the target JIRA server.", result.getMessage());
    }

    @Test
    public void testResponseError() throws Exception
    {
        StatusLine statusLine = mock(StatusLine.class);
        when(statusLine.getStatusCode()).thenReturn(500);
        HttpResponse response = mock(HttpResponse.class);
        when(response.getStatusLine()).thenReturn(statusLine);

        Result result = behaviour.handleResponse(response);
        assertFalse(result.isSuccess());
        assertEquals("Unexpected HTTP Status code 500 was returned.", result.getMessage());
    }
}
