package com.atlassian.bamboo.plugins.confdeploy;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Test harness for {@link UrlUtils}
 */
public class TestUrlUtlils
{
    /**
     * JUnit test boilerplate sucks. Let's do all the tests in one method. I hate myself.
     */
    @Test
    public void allTests()
    {
        assertEquals("https://extranet.atlassian.com/doauthenticate.action", UrlUtils.join("https://extranet.atlassian.com", "doauthenticate.action"));
        assertEquals("https://extranet.atlassian.com/doauthenticate.action", UrlUtils.join("https://extranet.atlassian.com", "/doauthenticate.action"));
        assertEquals("https://extranet.atlassian.com/doauthenticate.action", UrlUtils.join("https://extranet.atlassian.com/", "doauthenticate.action"));
        assertEquals("https://extranet.atlassian.com/doauthenticate.action", UrlUtils.join("https://extranet.atlassian.com/", "/doauthenticate.action"));

        assertEquals("https://extranet.atlassian.com/jira/login.jsp", UrlUtils.join("https://extranet.atlassian.com/jira", "login.jsp"));
        assertEquals("https://extranet.atlassian.com/jira/login.jsp", UrlUtils.join("https://extranet.atlassian.com/jira", "/login.jsp"));
        assertEquals("https://extranet.atlassian.com/jira/login.jsp", UrlUtils.join("https://extranet.atlassian.com/jira/", "login.jsp"));
        assertEquals("https://extranet.atlassian.com/jira/login.jsp", UrlUtils.join("https://extranet.atlassian.com/jira/", "/login.jsp"));

        assertEquals("http://confluence.atlassian.com", UrlUtils.getHostUrl("http://confluence.atlassian.com/"));
        assertEquals("http://confluence.atlassian.com", UrlUtils.getHostUrl("http://confluence.atlassian.com/display/SPCON"));
        assertEquals("https://pug.jira.com", UrlUtils.getHostUrl("https://pug.jira.com/wiki/doauthenticate.action"));
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testNotHttp()
    {
        UrlUtils.getHostUrl("ftp://ftp.atlassian.com/private");
    }
}
