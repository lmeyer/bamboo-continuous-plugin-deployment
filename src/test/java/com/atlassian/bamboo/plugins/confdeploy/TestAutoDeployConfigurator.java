package com.atlassian.bamboo.plugins.confdeploy;

import com.atlassian.bamboo.plugins.confdeploy.config.ui.ArtifactSubscriber;
import com.atlassian.bamboo.plugins.confdeploy.config.ui.SubscribedArtifact;
import com.atlassian.bamboo.plugins.confdeploy.config.ui.UiFields;
import com.atlassian.bamboo.webwork.util.WwSelectOption;
import com.atlassian.plugin.webresource.WebResourceManager;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

/**
 * Tests for {@link AutoDeployConfigurator}
 */
public class TestAutoDeployConfigurator
{
    private AutoDeployConfigurator autoDeployConfigurator;
    @Mock
    private ArtifactSubscriber artifactSubscriber;
    @Mock
    private WebResourceManager webResourceManager;

    @Before
    public void setUp()
    {
        MockitoAnnotations.initMocks(this);

        autoDeployConfigurator = new ConfluenceDeployConfigurator();
        autoDeployConfigurator.setArtifactSubscriber(artifactSubscriber);
        autoDeployConfigurator.setWebResourceManager(webResourceManager);
    }

    @Test
    public void testPopulateContextForCreate()
    {
        final Map<String,Object> context = Maps.newHashMap();
        SubscribedArtifact artifact1 = new SubscribedArtifact("id1", "group1", "name1");
        SubscribedArtifact artifact2 = new SubscribedArtifact("id2", "group2", "name2");
        when(artifactSubscriber.getSubscriptions(context)).thenReturn(Lists.newArrayList(artifact1, artifact2));

        autoDeployConfigurator.populateContextForCreate(context);

        assertTrue(context.containsKey("artifacts"));
        @SuppressWarnings("unchecked")
        List<WwSelectOption> actualArtifacts = (List<WwSelectOption>)context.get("artifacts");
        assertEquals(2, actualArtifacts.size());
        assertEquals(artifact1.getId(), actualArtifacts.get(0).getValue());
        assertEquals(artifact1.getGroup(), actualArtifacts.get(0).getGroup());
        assertEquals(artifact1.getDisplayName(), actualArtifacts.get(0).getDisplayName());

        assertEquals(artifact2.getId(), actualArtifacts.get(1).getValue());
        assertEquals(artifact2.getGroup(), actualArtifacts.get(1).getGroup());
        assertEquals(artifact2.getDisplayName(), actualArtifacts.get(1).getDisplayName());

        assertTrue(context.containsKey(UiFields.PLUGIN_ARTIFACT));
        WwSelectOption actualDefaultArtifact = (WwSelectOption)context.get(UiFields.PLUGIN_ARTIFACT);
        assertEquals(artifact1.getId(), actualDefaultArtifact.getValue());
        assertEquals(artifact1.getGroup(), actualDefaultArtifact.getGroup());
        assertEquals(artifact1.getDisplayName(), actualDefaultArtifact.getDisplayName());
    }

    @Test
    public void testPopulateContextForCreate_NoArtifacts()
    {
        final Map<String, Object> context = Maps.newHashMap();
        when(artifactSubscriber.getSubscriptions(context)).thenReturn(Lists.<SubscribedArtifact>newArrayList());

        autoDeployConfigurator.populateContextForCreate(context);

        assertTrue(context.containsKey("artifacts"));
        @SuppressWarnings("unchecked")
        List<WwSelectOption> actualArtifacts = (List<WwSelectOption>)context.get("artifacts");
        assertEquals(0, actualArtifacts.size());

        assertFalse(context.containsKey(UiFields.PLUGIN_ARTIFACT));
    }
}
