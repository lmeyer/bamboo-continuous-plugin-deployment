package com.atlassian.bamboo.plugins.confdeploy.config;

import com.atlassian.bamboo.configuration.ConfigurationMap;
import com.atlassian.bamboo.configuration.ConfigurationMapImpl;
import com.atlassian.bamboo.plugins.confdeploy.Crypto;
import com.atlassian.bamboo.plugins.confdeploy.Failure;
import com.atlassian.bamboo.plugins.confdeploy.Result;
import com.atlassian.bamboo.plugins.confdeploy.config.ui.ConfigFields;
import com.atlassian.fugue.Either;
import com.atlassian.fugue.Pair;
import com.google.common.collect.ImmutableMap;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.io.File;
import java.io.IOException;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Tests {@link DefaultTaskConfigurationFactory}.
 */
public class TestDefaultTaskConfigurationFactory
{
    private TaskConfigurationFactory factory;

    @Mock
    private ArtifactRetriever mockArtifactRetriever;


    @Before
    public void setUp()
    {
        MockitoAnnotations.initMocks(this);
        factory = new DefaultTaskConfigurationFactory(mockArtifactRetriever);
    }

    /**
     * Tests the factory logic with the minimal set of required parameters.
     * @throws IOException
     */
    @Test
    public void testGetTaskConfiguration() throws IOException
    {
        Pair<String,String> encryptedPassword = Crypto.encrypt("pwd001");
        final Map<String, String> config = ImmutableMap.of(
                ConfigFields.BASE_URL, "https://pug.jira.com/wiki",
                ConfigFields.USERNAME, "admin001",
                ConfigFields.PASSWORD, encryptedPassword.right(),
                ConfigFields.PASSWORD_ENCRYPTION_KEY, encryptedPassword.left(),
                ConfigFields.PLUGIN_ARTIFACT, "this-is-a-dummy-string"
        );
        final ConfigurationMap configurationMap = new ConfigurationMapImpl(config);
        File file = File.createTempFile("abc", "123");
        Mockito.when(mockArtifactRetriever.getFileFromArtifact("this-is-a-dummy-string")).thenReturn(Either.<File, Failure>left(file));

        Either<TaskConfiguration,Failure> maybeConfiguration = factory.getTaskConfiguration(configurationMap);
        assertTrue(maybeConfiguration.isLeft());

        TaskConfiguration configuration = maybeConfiguration.left().get();
        assertEquals("https://pug.jira.com/wiki", configuration.getRemoteBaseUrl());
        assertEquals("admin001", configuration.getUsername());
        assertEquals("pwd001", configuration.getPassword());
        assertEquals(file, configuration.getPluginJar());
    }

    /**
     * Tests the factory logic when the configured plugin artifact cannot be resolved.
     */
    @Test
    public void testGetTaskConfigurationInvalidArtifact()
    {
        Pair<String,String> encryptedPassword = Crypto.encrypt("pwd001");
        final Map<String, String> config = ImmutableMap.of(
                ConfigFields.BASE_URL, "https://pug.jira.com/wiki",
                ConfigFields.USERNAME, "admin001",
                ConfigFields.PASSWORD, encryptedPassword.right(),
                ConfigFields.PASSWORD_ENCRYPTION_KEY, encryptedPassword.left(),
                ConfigFields.PLUGIN_ARTIFACT, "this-is-a-dummy-string"
        );
        final ConfigurationMap configurationMap = new ConfigurationMapImpl(config);
        Failure expectedFailure = Result.failure("Your artifact is missing");
        Mockito.when(mockArtifactRetriever.getFileFromArtifact("this-is-a-dummy-string")).thenReturn(Either.<File, Failure>right(expectedFailure));

        Either<TaskConfiguration,Failure> maybeConfiguration = factory.getTaskConfiguration(configurationMap);
        assertTrue(maybeConfiguration.isRight());
        assertEquals(expectedFailure, maybeConfiguration.right().get());
    }

    /**
     * Tests the factory logic with a configuration for logging in via Atlassian ID
     * @throws IOException
     */
    @Test
    public void testGetTaskConfigurationWithAtlassianId() throws IOException
    {
        Pair<String, String> pwd = Crypto.encrypt("password1");
        Pair<String, String> pwd2 = Crypto.encrypt("password2");
        ImmutableMap.Builder<String, String> b = ImmutableMap.builder();
        b.put(ConfigFields.BASE_URL, "https://jira.atlassian.com");
        b.put(ConfigFields.USERNAME, "jclark");
        b.put(ConfigFields.PASSWORD_ENCRYPTION_KEY, pwd.left());
        b.put(ConfigFields.PASSWORD, pwd.right());
        b.put(ConfigFields.USE_ATLASSIAN_ID, Boolean.TRUE.toString());
        b.put(ConfigFields.ATLASSIAN_ID_BASE_URL, "https://id.atlassian.com");
        b.put(ConfigFields.ATLASSIAN_ID_USERNAME, "jclark@atlassian.com");
        b.put(ConfigFields.ATLASSIAN_ID_PASSWORD_ENCRYPTION_KEY, pwd2.left());
        b.put(ConfigFields.ATLASSIAN_ID_PASSWORD, pwd2.right());
        b.put(ConfigFields.PLUGIN_ARTIFACT, "this-is-my-plugin-artifact");

        final ConfigurationMap configurationMap = new ConfigurationMapImpl(b.build());

        File expectedPluginJar = File.createTempFile("abc", "123");
        Mockito.when(mockArtifactRetriever.getFileFromArtifact("this-is-my-plugin-artifact")).thenReturn(Either.<File, Failure>left(expectedPluginJar));

        Either<TaskConfiguration,Failure> maybeConfiguration = factory.getTaskConfiguration(configurationMap);
        assertTrue(maybeConfiguration.isLeft());

        TaskConfiguration configuration = maybeConfiguration.left().get();
        assertEquals("https://jira.atlassian.com", configuration.getRemoteBaseUrl());
        assertTrue(configuration.useAtlassianId());
        assertEquals("https://id.atlassian.com", configuration.getAtlassianIdBaseUrl());
        assertEquals("jclark@atlassian.com", configuration.getAtlassianIdUsername());
        assertEquals("password2", configuration.getAtlassianIdPassword());
        assertEquals(expectedPluginJar, configuration.getPluginJar());
    }

    /**
     * Tests the factory logic with a configuration for logging in via Atlassian ID, the Atlassian ID Base URL is not
     * specified and should default to the production host URL.
     *
     * @throws IOException
     */
    @Test
    public void testGetTaskConfigurationWithAtlassianIdDefaultUrl() throws IOException
    {
        // TODO: Don't duplicate all this code from the previous test.
        Pair<String, String> pwd = Crypto.encrypt("password1");
        Pair<String, String> pwd2 = Crypto.encrypt("password2");
        ImmutableMap.Builder<String, String> b = ImmutableMap.builder();
        b.put(ConfigFields.BASE_URL, "https://jira.atlassian.com");
        b.put(ConfigFields.USERNAME, "jclark");
        b.put(ConfigFields.PASSWORD_ENCRYPTION_KEY, pwd.left());
        b.put(ConfigFields.PASSWORD, pwd.right());
        b.put(ConfigFields.USE_ATLASSIAN_ID, Boolean.TRUE.toString());
        b.put(ConfigFields.ATLASSIAN_ID_USERNAME, "jclark@atlassian.com");
        b.put(ConfigFields.ATLASSIAN_ID_PASSWORD_ENCRYPTION_KEY, pwd2.left());
        b.put(ConfigFields.ATLASSIAN_ID_PASSWORD, pwd2.right());
        b.put(ConfigFields.PLUGIN_ARTIFACT, "this-is-my-plugin-artifact");

        final ConfigurationMap configurationMap = new ConfigurationMapImpl(b.build());

        File expectedPluginJar = File.createTempFile("abc", "123");
        Mockito.when(mockArtifactRetriever.getFileFromArtifact("this-is-my-plugin-artifact")).thenReturn(Either.<File, Failure>left(expectedPluginJar));

        Either<TaskConfiguration,Failure> maybeConfiguration = factory.getTaskConfiguration(configurationMap);
        assertTrue(maybeConfiguration.isLeft());

        TaskConfiguration configuration = maybeConfiguration.left().get();
        assertEquals("https://jira.atlassian.com", configuration.getRemoteBaseUrl());
        assertTrue(configuration.useAtlassianId());
        assertEquals("https://id.atlassian.com", configuration.getAtlassianIdBaseUrl());
        assertEquals("jclark@atlassian.com", configuration.getAtlassianIdUsername());
        assertEquals("password2", configuration.getAtlassianIdPassword());
        assertEquals(expectedPluginJar, configuration.getPluginJar());
    }
}
