package com.atlassian.bamboo.plugins.confdeploy.upload.behaviour.upm;

import com.atlassian.bamboo.plugins.confdeploy.Failure;
import com.atlassian.bamboo.plugins.confdeploy.Result;
import com.atlassian.bamboo.plugins.confdeploy.config.TaskConfiguration;
import com.atlassian.bamboo.plugins.confdeploy.upload.behaviour.RequestBehaviour;
import com.atlassian.fugue.Either;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.message.BasicHeader;
import org.junit.Before;
import org.junit.Test;
import testsupport.MockTaskConfigurationFactory;

import java.io.IOException;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static testsupport.MockTaskConfigurationFactory.BASE_URL;

/**
 * Tests for {@link TestUpmTokenRequestBehaviour}.
 */
public class TestUpmTokenRequestBehaviour
{
    private static final String TEST_BASE_URL = "https://pug.jira.com/wiki";

    private RequestBehaviour requestBehaviour;
    private TaskConfiguration mockConfig;

    @Before
    public void setUp()
    {
        requestBehaviour = new UpmTokenRequestBehaviour();
    }

    @Test
    public void testGetRequest()
    {
        mockConfig = MockTaskConfigurationFactory.getMockTaskConfiguration(ImmutableMap.<String, Object>of(
           BASE_URL, TEST_BASE_URL
        ));

        Either<HttpUriRequest,Failure> maybeRequest = requestBehaviour.getRequest(mockConfig, Maps.<String, Object>newHashMap());
        assertTrue(maybeRequest.isLeft());

        HttpUriRequest request = maybeRequest.left().get();
        // this is the important bit! UPM REST API uses custom media types.
        assertEquals("application/vnd.atl.plugins.installed+json", request.getHeaders("Accept")[0].getValue());
    }

    @Test
    public void testResponseSuccess() throws IOException
    {
        HttpResponse response = mock(HttpResponse.class);
        when(response.getHeaders("upm-token")).thenReturn(new BasicHeader[] {new BasicHeader("upm-token", "the upm token")});

        Result result = requestBehaviour.handleResponse(response);
        assertTrue(result.isSuccess());
        assertNotNull(result.getContext());
        assertTrue(result.getContext() instanceof String);
        assertEquals("the upm token", result.getContext().toString());
    }

    @Test
    public void testResponseFailure() throws IOException
    {
        HttpResponse response = mock(HttpResponse.class);
        when(response.getHeaders("upm-token")).thenReturn(null);

        Result result = requestBehaviour.handleResponse(response);
        assertFalse(result.isSuccess());
        assertNull(result.getContext());
    }
}
