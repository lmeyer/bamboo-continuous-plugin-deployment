package com.atlassian.bamboo.plugins.confdeploy.upload.behaviour.upm;

import com.atlassian.bamboo.plugins.confdeploy.Failure;
import com.atlassian.bamboo.plugins.confdeploy.Result;
import com.atlassian.bamboo.plugins.confdeploy.config.TaskConfiguration;
import com.atlassian.bamboo.plugins.confdeploy.upload.behaviour.RequestBehaviour;
import com.atlassian.fugue.Either;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.StringEntity;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import testsupport.MockTaskConfigurationFactory;
import testsupport.assertions.HttpUriRequestAssertion;
import testsupport.assertions.HttpUriRequestAssertionHelper;
import testsupport.assertions.MethodTypeAssertion;
import testsupport.assertions.RequestUrlAssertion;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static testsupport.MockTaskConfigurationFactory.BASE_URL;
import static testsupport.MockTaskConfigurationFactory.PLUGIN_JAR;

/**
 * Tests for {@link PluginUploadRequestBehaviour}.
 */
public class TestPluginUploadRequestBehaviour
{
    private static final String TEST_BASE_URL = "http://localhost:2990/jira";

    private RequestBehaviour requestBehaviour;
    private TaskConfiguration mockConfig;

    @Before
    public void setUp()
    {
        requestBehaviour = new PluginUploadRequestBehaviour();
    }

    @Test
    public void testUpmTokenIsRequired() throws IOException
    {
        mockConfig = MockTaskConfigurationFactory.getMockTaskConfiguration(ImmutableMap.<String, Object>of(
                PLUGIN_JAR, File.createTempFile("abc", "123"),
                BASE_URL, TEST_BASE_URL
        ));

        Either<HttpUriRequest,Failure> maybeRequest = requestBehaviour.getRequest(mockConfig, Maps.<String, Object>newHashMap());
        assertTrue(maybeRequest.isRight());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testBaseUrlIsRequired() throws IOException
    {
        mockConfig = MockTaskConfigurationFactory.getMockTaskConfiguration(ImmutableMap.<String, Object>of(
                PLUGIN_JAR, File.createTempFile("abc", "123"),
                BASE_URL, ""
        ));

        HashMap<String,Object> requestContext = Maps.newHashMap();
        requestContext.put("upmToken", "This is a real upm token, honest!");

        requestBehaviour.getRequest(mockConfig, requestContext); // should throw
    }

    @Test(expected = NullPointerException.class)
    public void testPluginJarIsRequired()
    {
        mockConfig = MockTaskConfigurationFactory.getMockTaskConfiguration(ImmutableMap.<String, Object>of(
                PLUGIN_JAR, null,
                BASE_URL, TEST_BASE_URL
        ));

        HashMap<String,Object> requestContext = Maps.newHashMap();
        requestContext.put("upmToken", "This is a real upm token, honest!");

        requestBehaviour.getRequest(mockConfig, requestContext); // should throw
    }

    @Test
    public void testGetRequest() throws IOException
    {
        mockConfig = MockTaskConfigurationFactory.getMockTaskConfiguration(ImmutableMap.<String, Object>of(
           PLUGIN_JAR, File.createTempFile("abc", "123"),
           BASE_URL, TEST_BASE_URL
        ));

        Map<String, Object> requestContext = Maps.newHashMap();
        requestContext.put("upmToken", "mahtoken");

        Either<HttpUriRequest, Failure> maybeRequest = requestBehaviour.getRequest(mockConfig, requestContext);
        assertTrue(maybeRequest.isLeft());

        HttpUriRequest request = maybeRequest.left().get();
        HttpUriRequestAssertionHelper.assertRequest(request, ImmutableList.<HttpUriRequestAssertion>of(
                new MethodTypeAssertion(MethodTypeAssertion.Method.POST),
                new RequestUrlAssertion(TEST_BASE_URL + "/rest/plugins/1.0/?token=mahtoken")
                // Unit testing the multipart form entity that is POSTed with this request too much of a challenge for now, ignoring
        ));
    }

    @Test
    public void testResponseSuccess() throws IOException, JSONException
    {
        StatusLine statusLine = mock(StatusLine.class);
        when(statusLine.getStatusCode()).thenReturn(202);
        HttpResponse response = mock(HttpResponse.class);
        when(response.getStatusLine()).thenReturn(statusLine);
        when(response.getEntity()).thenReturn(new StringEntity("{\"test\": \"yes\"}"));

        Result result = requestBehaviour.handleResponse(response);
        assertTrue(result.isSuccess());

        assertNotNull(result.getContext());
        assertTrue(result.getContext() instanceof JSONObject);
        JSONObject j = (JSONObject)result.getContext();
        assertEquals("yes", j.getString("test"));
    }

    @Test
    public void testResponseFailure() throws IOException
    {
        StatusLine statusLine = mock(StatusLine.class);
        when(statusLine.getStatusCode()).thenReturn(500);
        HttpResponse response = mock(HttpResponse.class);
        when(response.getStatusLine()).thenReturn(statusLine);

        Result result = requestBehaviour.handleResponse(response);
        assertFalse(result.isSuccess());
    }

}
