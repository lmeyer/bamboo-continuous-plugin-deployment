package com.atlassian.bamboo.plugins.confdeploy.upload.behaviour;

import com.atlassian.bamboo.plugins.confdeploy.Failure;
import com.atlassian.bamboo.plugins.confdeploy.Result;
import com.atlassian.bamboo.plugins.confdeploy.config.TaskConfiguration;
import com.atlassian.bamboo.plugins.confdeploy.upload.behaviour.product.ConfluenceWebSudoRequestBehaviour;
import com.atlassian.bamboo.plugins.confdeploy.upload.behaviour.product.JiraWebSudoRequestBehaviour;
import com.atlassian.fugue.Either;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicNameValuePair;
import org.junit.Test;
import testsupport.MockTaskConfigurationFactory;
import testsupport.assertions.FormEncodedRequestBodyAssertion;
import testsupport.assertions.HttpUriRequestAssertion;
import testsupport.assertions.HttpUriRequestAssertionHelper;
import testsupport.assertions.RequestUrlAssertion;

import static org.junit.Assert.assertTrue;
import static testsupport.MockTaskConfigurationFactory.PASSWORD;
import static org.mockito.Mockito.*;

/**
 * Tests for {@link ProductWebSudoRequestBehaviour} and its known subclasses.
 */
public class TestProductWebSudoRequestBehaviour
{
    private static final String TEST_BASE_URL = "http://something.atlassian.com";
    private static final String TEST_PASSWORD = "myonlinebankingpassword";

    @Test
    public void testGetRequest_Confluence()
    {
        RequestBehaviour cwsrb = new ConfluenceWebSudoRequestBehaviour(TEST_BASE_URL);

        TaskConfiguration mockConfig = MockTaskConfigurationFactory.getMockTaskConfiguration(ImmutableMap.<String, Object>of(
                PASSWORD, TEST_PASSWORD
        ));

        Either<HttpUriRequest,Failure> maybeRequest = cwsrb.getRequest(mockConfig, Maps.<String, Object>newHashMap());
        assertTrue(maybeRequest.isLeft());

        HttpUriRequestAssertionHelper.assertRequest(maybeRequest.left().get(), ImmutableList.<HttpUriRequestAssertion>of(
                new RequestUrlAssertion(TEST_BASE_URL + "/doauthenticate.action"),
                new FormEncodedRequestBodyAssertion(ImmutableList.<NameValuePair>of(
                        new BasicNameValuePair("password", TEST_PASSWORD),
                        new BasicNameValuePair("authenticate", "Confirm")
                ))
        ));
    }

    @Test
    public void testGetRequest_Jira()
    {
        RequestBehaviour jwsrb = new JiraWebSudoRequestBehaviour(TEST_BASE_URL);

        TaskConfiguration mockConfig = MockTaskConfigurationFactory.getMockTaskConfiguration(ImmutableMap.<String, Object>of(
                PASSWORD, TEST_PASSWORD
        ));

        Either<HttpUriRequest,Failure> maybeRequest = jwsrb.getRequest(mockConfig, Maps.<String, Object>newHashMap());
        assertTrue(maybeRequest.isLeft());

        HttpUriRequestAssertionHelper.assertRequest(maybeRequest.left().get(), ImmutableList.<HttpUriRequestAssertion>of(
                new RequestUrlAssertion(TEST_BASE_URL + "/secure/admin/WebSudoAuthenticate.jspa"),
                new FormEncodedRequestBodyAssertion(ImmutableList.<NameValuePair>of(
                        new BasicNameValuePair("webSudoPassword", TEST_PASSWORD),
                        new BasicNameValuePair("authenticate", "Confirm")
                ))
        ));
    }

    @Test
    public void testHandleResponse_Success() throws Exception
    {
        HttpResponse mockResponse = mock(HttpResponse.class);
        when(mockResponse.getHeaders("X-Atlassian-WebSudo")).thenReturn(new BasicHeader[] {new BasicHeader("X-Atlassian-WebSudo", "Has-Authentication")});

        RequestBehaviour jwsrb = new JiraWebSudoRequestBehaviour(TEST_BASE_URL);

        Result result = jwsrb.handleResponse(mockResponse);
        assertTrue(result.isSuccess());
    }
}
